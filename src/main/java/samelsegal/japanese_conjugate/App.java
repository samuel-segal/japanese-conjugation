package samelsegal.japanese_conjugate;


import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import samelsegal.japanese_conjugate.conjugation.JapaneseConjugationDict;
import samelsegal.japanese_conjugate.words.JapaneseDict;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
    	JapaneseHandler handler = new JapaneseHandler();
    	handler.load();
        
        

        var label = new Label("Text");
        var scene = new Scene(new StackPane(label), 640, 480);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
        	switch(key.getCode()) {
        	case ENTER:
        	case SPACE:
        		handler.flip();
        		label.setText( handler.current() );
        		break;
        	case LEFT:
        	case RIGHT:
        		handler.nextWord();
        		label.setText( handler.current());
        		break;
        	case UP:
        	case DOWN:
        		
        		handler.nextConj();
        		label.setText( handler.current() );
        		break;
			default:
				break;
        		
        	}
        });
        
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
        
    }

}