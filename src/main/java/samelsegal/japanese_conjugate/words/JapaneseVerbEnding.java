package samelsegal.japanese_conjugate.words;

import java.util.Map;

public class JapaneseVerbEnding {
	
	public VerbType japaneseVerbType;
	
	public static enum VerbType{
		RU1,
		RU5,
		U,
		KU,
		GU,
		SU,
		TSU,
		BU,
		NU,
		MU
	}
	private static Map<String,VerbType> verbMap = Map.ofEntries(
			Map.entry("RU1",VerbType.RU1),
			Map.entry("RU5",VerbType.RU5),
			Map.entry("U",VerbType.U),
			Map.entry("KU",VerbType.KU),
			Map.entry("GU",VerbType.GU),
			Map.entry("SU",VerbType.SU),
			Map.entry("TSU",VerbType.TSU),
			Map.entry("BU",VerbType.BU),
			Map.entry("NU",VerbType.NU),
			Map.entry("MU",VerbType.MU)
			);
			

	public static VerbType verbTypeFromString(String s) {
		return verbMap.get(s);
	}
	
	
	public String asTeForm() {
		switch(japaneseVerbType) {
		case RU1:
			return "て";
		case U:
		case TSU:
		case RU5:
			return "って";
		case MU:
		case NU:
		case BU:
			return "んで";
		case KU:
			return "いて";
		case GU:
			return "いで";
		case SU:
			return "して";
		//Default is te-form of する
		default:
			return "して";
		}

	}
	public String typeAsString(char type) {
		if(this.japaneseVerbType == VerbType.RU1) {
			switch(type) {
			case 'e':
				return "られ";
			case 'u':
				return "る";
			default:
				return "";
			}
		}
		else {
			return String.valueOf( typeAsChar(type) );
		}
	}
	
	private char typeAsChar() {
		switch(this.japaneseVerbType) {
		case RU1:
		case RU5:
			return 'る';
		case U:
			return 'う';
		case KU:
			return 'く';
		case GU:
			return 'ぐ';
		case SU:
			return 'す';
		case TSU:
			return 'つ';
		case BU:
			return 'ぶ';
		case NU:
			return 'ぬ';
		case MU:
			return 'む';
		//Returns a character that should never occur in standard Japanese
		default:
			return 'ゑ';
		}
	}
	private char typeAsChar(char type) {
		int unicodeTranslate = 0;
		switch(type) {
		case 'a':
			unicodeTranslate = -2;
			break;
		case 'i':
			unicodeTranslate = -1;
			break;
		case 'e':
			unicodeTranslate = 1;
			break;
		case 'o':
			unicodeTranslate = 2;
		}
		//Some areas in the japanese unicode have different seperations
		switch(this.japaneseVerbType) {
		case KU:
		case GU:
		case SU:
		case U:
			unicodeTranslate*=2;
			break;
		case BU:
			unicodeTranslate*=3;
			break;
		case TSU:
			unicodeTranslate*=2;
			if(unicodeTranslate<0) {
				unicodeTranslate--;
			}
		default:
			break;
		}
		char resultChar = (char)(this.typeAsChar() + unicodeTranslate);
		//Japanese grammer: あ　gets replaced with わ
		resultChar = (resultChar=='あ')?'わ':resultChar;
		return resultChar;
	}

	@Override
	public String toString() {
		return typeAsString('u');
	}
	
}
