package samelsegal.japanese_conjugate.words;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JapaneseDict {
	
	public List<JapaneseWord> arrayList = new ArrayList<JapaneseWord>();
	
	public void loadFile(String filename) throws IOException {
		File f = new File(filename);
		Scanner s = new Scanner(f);
		while(s.hasNextLine()) {
			String line = s.nextLine();
			String[] values = line.split("/");
			String english = values[0];
			String japaneseStem = values[1];
			String verbString = values[2];
			
			
			
			JapaneseWord word = new JapaneseWord();
			word.englishWord = english;
			word.japaneseStem = japaneseStem;
			
			JapaneseVerbEnding verb = new JapaneseVerbEnding();
			verb.japaneseVerbType = JapaneseVerbEnding.verbTypeFromString(verbString);
			word.japaneseVerbEnding = verb;
			
			arrayList.add(word);
		}
		
		s.close();
	}
	
	
	public JapaneseWord getRandomWord() {
		int index = (int)(Math.random()*arrayList.size());
		return arrayList.get(index);
	}
	
}
