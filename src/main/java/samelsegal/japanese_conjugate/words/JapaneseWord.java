package samelsegal.japanese_conjugate.words;

public class JapaneseWord {
	public String englishWord;
	
	public String japaneseStem;
	
	public JapaneseVerbEnding japaneseVerbEnding;
	
	

	public String asTeForm() {
		return this.japaneseStem + japaneseVerbEnding.asTeForm();
	}
	
	public String asForm(char c) {
		return this.japaneseStem + japaneseVerbEnding.typeAsString(c);
	}
	
	@Override
	public String toString() {
		return this.japaneseStem + japaneseVerbEnding;
	}
	
	
	
	
}
