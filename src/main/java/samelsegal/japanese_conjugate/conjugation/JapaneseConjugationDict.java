package samelsegal.japanese_conjugate.conjugation;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class JapaneseConjugationDict {
	public List<JapaneseConjugator> conjugateList = new LinkedList<JapaneseConjugator>();
	
	
	public void loadFile(String filename) throws IOException{
		File f = new File(filename);
		Scanner s = new Scanner(f);
		while(s.hasNextLine()) {
			String line = s.nextLine();
			String[] values = line.split("/");
			String english = values[0];
			String japanese = values[1];
			JapaneseConjugator conjugator = new JapaneseConjugator(english,japanese);
			conjugateList.add(conjugator);
		}
		s.close();
	}
	
	
	
	public JapaneseConjugator getRandom() {
		int index = (int)(Math.random()*conjugateList.size());
		return conjugateList.get(index);
	}
}
