package samelsegal.japanese_conjugate.conjugation;

import samelsegal.japanese_conjugate.words.JapaneseVerbEnding;
import samelsegal.japanese_conjugate.words.JapaneseWord;

public class JapaneseConjugator {
	
	protected JapaneseWord word;
	
	protected String englishPattern;
	protected String japanesePattern;
	
	public JapaneseConjugator(String englishPattern, String japaensePattern) {
		this.englishPattern = englishPattern;
		this.japanesePattern = japaensePattern;
	}
	
	
	public String getEnglish() {
		return this.englishPattern.replace("$", word.englishWord);
	}
	
	
	
	public String getJapanese() {
		char[] vowels = {'a','e','i','o','u'};
		
		String pattC = japanesePattern;
		for(char c:vowels) {
			pattC = pattC.replace("$"+c, word.asForm(c));
		}
		pattC = pattC.replace("$t", word.asTeForm());
		if(
			word.japaneseVerbEnding.japaneseVerbType == JapaneseVerbEnding.VerbType.RU1	
			) {
			pattC = pattC.replaceAll("\\?r", "");
		}
		else {
			pattC = pattC.replaceAll("\\?r.", "");
		}
		return pattC;
	}
	
	
	public void setWord(JapaneseWord word) {
		this.word = word;
	}
	
	
}
