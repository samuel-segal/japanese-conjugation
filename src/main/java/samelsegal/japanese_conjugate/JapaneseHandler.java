package samelsegal.japanese_conjugate;

import java.io.IOException;

import samelsegal.japanese_conjugate.conjugation.JapaneseConjugationDict;
import samelsegal.japanese_conjugate.conjugation.JapaneseConjugator;
import samelsegal.japanese_conjugate.words.JapaneseDict;
import samelsegal.japanese_conjugate.words.JapaneseWord;

public class JapaneseHandler {

	JapaneseConjugationDict jConDict = new JapaneseConjugationDict();
	JapaneseDict jDict = new JapaneseDict();

	JapaneseConjugator curConj;
	
	boolean isEnglish = false;
	
	
	public void load() {
		try {
			jConDict.loadFile("conjugate");
			jDict.loadFile("dict");
		}catch(IOException e) {
			e.printStackTrace();
		}
		curConj = jConDict.getRandom();
		curConj.setWord( jDict.getRandomWord() );
		
	}

	
	public void flip() {
		isEnglish = !isEnglish;
	}
	
	public void nextWord() {
		JapaneseWord randWord = jDict.getRandomWord();
		curConj.setWord(randWord);
	}
	
	public void nextConj() {
		curConj = jConDict.getRandom();
		nextWord();
	}
	
	public String current() {
		return isEnglish ? curConj.getEnglish() : curConj.getJapanese();
	}
	
	
	
}
